import React from 'react';
import './Product.css';

const Items = props =>{
    return(
        <div className='Form'>
            <div className='Add'>
                <button className='Okay' onClick={() => props.click(props.name)}>
                    <h5> {props.name} </h5>
                    <p> Цена: {props.price} KGS</p>
                </button>
            </div>
        </div>
    )

};

export default Items;