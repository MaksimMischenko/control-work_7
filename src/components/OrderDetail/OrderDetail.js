import React from 'react';
import './OrderDetail.css';


const OrderDetail = (props) => {
    return (
        props.item.map((items, i) => {
            return (
                items.count === 0 ?
                    null :
                    <div key={i}>
                        <div className="Order">
                            <h1>{items.name}</h1>
                            <p>Кол-во: {items.count}</p>
                            <p>KGS {items.price}</p>
                            <div className='Btn'>
                                <button className='Btn-delete' onClick={() => props.remove(items.name)}>✖</button>
                            </div>
                        </div>
                    </div>
            )
        })
    )
};

export default OrderDetail;