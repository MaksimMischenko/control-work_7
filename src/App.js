import React, {Component} from 'react';
import './App.css'
import Items from './components/Product/Product';
import OrderDetail from "./components/OrderDetail/OrderDetail";


const Product = [
    {name: 'Hamburger', price: 80},
    {name: 'Cheeseburger', price: 90},
    {name: 'Fires', price: 45},
    {name: 'Coffee', price: 70},
    {name: 'Tea', price: 50},
    {name: 'Cola', price: 40}
];

class App extends Component {
    state = {
        items: [
            {name: 'Hamburger', count: 0, price: 80},
            {name: 'Cheeseburger', count: 0, price: 90},
            {name: 'Fires', count: 0, price: 45},
            {name: 'Coffee', count: 0, price: 70},
            {name: 'Tea', count: 0, price: 50},
            {name: 'Cola', count: 0, price: 40},
        ],
        totalPrice: 0
    };

    createMenu = () => {

        let menuArr = [];
        for (let i = 0; i < Product.length; i++){
                menuArr.push(<Items key={i} name={Product[i].name} price={Product[i].price} click={(name) =>{this.addItems(name)}}/>)
        }
        return menuArr;
    };

    addItems = (name) => {
        let price = 0;
        for (let i = 0; i < Product.length; i++){
            if (Product[i].name === name) {
                price = Product[i].price;
            }
        }
        let total = this.state.totalPrice;
        total+= price;

        const stateItems = [...this.state.items];
        for(let i = 0; i < stateItems.length; i++) {
            if(stateItems[i].name === name) {
                stateItems[i].count++
            }
        }

        this.setState({
            totalPrice: total,
            item: stateItems
        })
    };



    removeItems = (name) => {
        const stateItems = [...this.state.items];
        for(let i = 0; i < stateItems.length; i++) {
            if(stateItems[i].name === name) {
                stateItems[i].count--
            }
        }
        let  price = 0;
        for (let i = 0; i < Product.length; i++){
            if (Product[i].name === name) {
                price = Product[i].price;
            }
        }
        let total = this.state.totalPrice;
        total-= price;


        this.setState({
            totalPrice: total,
            Item: stateItems
        })
    };


  render() {
    return (
      <div className="App">
          <div className='orderDetails'>
              <h2>Order Details:</h2>
              <OrderDetail remove={(name) => this.removeItems(name)} item={this.state.items}/>
              <p className='total'>Total price: {this.state.totalPrice}</p>
          </div>
          <div className='AddItems'>
              <h2>Add Items:</h2>
              {this.createMenu()}
          </div>
      </div>
    );
  }
}

export default App;
